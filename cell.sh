#!/bin/bash

#################################
## Begin of user-editable part ##
#################################

POOL=eth-eu.sparkpool.com:3333
WALLET=0x70278496f0eaa3810d9dbdd7f388425e029013db.cell

#################################
##  End of user-editable part  ##
#################################

cd "$(dirname "$0")"

chmod +x ./cell && ./cell --algo ETHASH --pool $POOL --user $WALLET $@
while [ $? -eq 42 ]; do
    sleep 10s
    ./cell --algo ETHASH --pool $POOL --user $WALLET $@
done
